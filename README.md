# OpenML dataset: Obesity

https://www.openml.org/d/45969

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Description:
The dataset named "ObesityDataSet_raw_and_data_sinthetic.csv" comprises comprehensive attributes aimed at the analysis and prediction of obesity levels among individuals. It captures a diverse range of variables including Age, Gender, Height, Weight, eating habits (CALC, FAVC, FCVC, NCP, SCC, SMOKE, CH2O, CAEC), physical activity level (FAF), usage time of technology devices (TUE), modes of transportation (MTRANS), and the family history of being overweight. The dataset doesn't only account for quantitative metrics but also integrates qualitative assessments, providing a holistic view of factors contributing to obesity. With entries from various age groups and both genders, this dataset encapsulates a detailed representation of lifestyle choices and their potential impact on weight categories, classified under the attribute 'NObeyesdad'.

Attribute Description:
- Age: Numeric, represents the age of the individual.
- Gender: Categorical, includes 'Female' and 'Male'.
- Height: Numeric, indicates the height in meters.
- Weight: Numeric, specifies the weight in kilograms.
- CALC: Categorical, captures the frequency of alcohol consumption.
- FAVC: Binary, indicates the consumption of high caloric food frequently.
- FCVC: Numeric, frequency of vegetable consumption.
- NCP: Numeric, average number of main meals.
- SCC: Binary, indicates if the individual consults a calorie consumption monitoring.
- SMOKE: Binary, represents smoking habits.
- CH2O: Numeric, daily water consumption in liters.
- family_history_with_overweight: Binary, indicates a family history of overweight.
- FAF: Numeric, frequency of physical activity per week.
- TUE: Numeric, time using technology devices in hours.
- CAEC: Categorical, consumption of food between meals.
- MTRANS: Categorical, usual mode of transportation.
- NObeyesdad: Categorical, denotes the obesity level of the individual.

Use Case:
This dataset is instrumental for researchers and healthcare professionals aiming to explore the relationships between lifestyle choices, genetic predispositions, and obesity. It supports predictive modeling to identify at-risk individuals based on their habits and personal attributes. Besides academic and clinical research, it can also be utilized by public health organizations to design targeted interventions and awareness campaigns catered to reducing obesity prevalence by addressing modifiable risk factors.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45969) of an [OpenML dataset](https://www.openml.org/d/45969). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45969/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45969/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45969/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

